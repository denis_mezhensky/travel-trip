<?php
namespace TravelTrip\Test;

use TravelTrip\Trips\TrainTrip;

class TrainTripTest extends \PHPUnit_Framework_TestCase
{
    public function test_train()
    {
        $trainTrip = new TrainTrip('Madrid', 'Casablanca', '45B', '78A');

        $this->assertEquals($trainTrip->toString(),"Take train 78A from Madrid to Casablanca. Sit in seat 45B.");
    }
}

