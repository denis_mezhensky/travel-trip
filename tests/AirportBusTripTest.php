<?php

namespace TravelTrip\Test;

use TravelTrip\Trips\AirportBusTrip;

class AirportBusTripTest extends \PHPUnit_Framework_TestCase
{
    public function test_airport_bus()
    {
        $airportBusTrip = new AirportBusTrip('Casablanca', 'Gerona Airport');

        $this->assertEquals($airportBusTrip->toString(),"Take the airport bus from Casablanca to Gerona Airport. No seat assignment.");
    }
}

