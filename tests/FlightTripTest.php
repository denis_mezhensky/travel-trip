<?php
namespace TravelTrip\Test;

use TravelTrip\Trips\FlightTrip;

class FlightTripTest extends \PHPUnit_Framework_TestCase
{
    public function test_flight()
    {
        $flightTrip = new FlightTrip('Stockholm', 'New York JFK', '7B', 'SK22', '22');

        $this->assertEquals($flightTrip->toString(),"From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B. Baggage will be automatically transferred from your last leg.");
    }

}
