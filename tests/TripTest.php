<?php
namespace TravelTrip\Test;

use TravelTrip\Trips\AirportBusTrip;
use TravelTrip\Trips\FlightTrip;
use TravelTrip\Trips\TrainTrip;
use TravelTrip\Trip;

class TripTest extends \PHPUnit_Framework_TestCase
{
    public function test_trip()
    {
        $trip = new  Trip();

        $trip->addBordingCard(new FlightTrip('Stockholm', 'New York JFK', '7B', 'SK22', '22'));
        $trip->addBordingCard(new FlightTrip('Gerona Airport', 'Stockholm', '3A', 'SK455', '45B', '344'));
        $trip->addBordingCard(new TrainTrip('Madrid', 'Casablanca', '45B', '78A'));
        $trip->addBordingCard(new AirportBusTrip('Casablanca', 'Gerona Airport'));

        $trip->sortBordingCard();

        $result = "<ul>".
                     "<li>Take train 78A from Madrid to Casablanca. Sit in seat 45B.</li>".
                     "<li>Take the airport bus from Casablanca to Gerona Airport. No seat assignment.</li>".
                     "<li>From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A. Baggage drop at ticket counter 344.</li>".
                     "<li>From Stockholm, take flight SK22 to New York JFK. Gate 22, seat 7B. Baggage will be automatically transferred from your last leg.</li><li>You have arrived at your final destination.</li>".
                  "</ul>";

        $this->assertEquals($trip->toHtml(), $result);
    }
}

