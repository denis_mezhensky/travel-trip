Install the PHP dependencies (phpunit, phpdoc) and generate the autoloading file.

```
composer install
```

Browse the package url via browser, It should output sorted list.


### Dependencies
- PHP 5.6


