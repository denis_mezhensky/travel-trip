<?php
namespace TravelTrip;

require_once __DIR__ . '/vendor/autoload.php';

use TravelTrip\Trips\AirportBusTrip;
use TravelTrip\Trips\FlightTrip;
use TravelTrip\Trips\TrainTrip;


$trip = new Trip();

$trip->addBordingCard(new FlightTrip('Stockholm', 'New York JFK', '7B', 'SK22', '22'));
$trip->addBordingCard(new TrainTrip('Madrid', 'Barcelona', '45B', '78A'));
$trip->addBordingCard(new FlightTrip('Gerona Airport', 'Stockholm', '3A', 'SK455', '45B', '344'));
$trip->addBordingCard(new AirportBusTrip('Barcelona', 'Gerona Airport'));

$trip->sortBordingCard();

echo($trip->toHtml());

