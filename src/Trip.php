<?php
namespace TravelTrip;

use TravelTrip\Trips\AbstractTrip;
use TravelTrip\Utils\TripSorter;

class Trip
{
    private $boardingCard;

    public function getBoardingCards()
    {
        return $this->boardingCard;
    }

    private $sortedBaseBoardingCard;

    public function sortBordingCard(){
        $this->sortedBaseBoardingCard = (new TripSorter)->sort($this->boardingCard);
    }

    public function addBordingCard(AbstractTrip $boardingCard){
        $this->boardingCard[] = $boardingCard;
    }

    public function toHtml()
    {
        $str = '<ul>';
        foreach( $this->sortedBaseBoardingCard as $boarding){
            $str .= '<li>' . $boarding->toString() . '</li>' ;
        }
        $str .= '<li>You have arrived at your final destination.</li>';
        $str .= '</ul>';
        return $str;
    }
}

