<?php
namespace TravelTrip\Trips;

abstract class AbstractTrip
{
    protected $from, $to, $seat;

    function __construct($from, $to, $seat)
    {
        $this->from = $from;
        $this->to = $to;
        $this->seat = $seat;
    }

    public function get($name)
    {
        return $this->$name;
    }
}
