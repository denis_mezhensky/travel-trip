<?php
namespace TravelTrip\Trips;

class FlightTrip extends AbstractTrip
{

    private $flight, $gate, $counter;

    function __construct($from, $to, $seat, $flight, $gate, $counter = null)
    {
        parent::__construct($from, $to, $seat);

        $this->flight = $flight;
        $this->gate = $gate;
        $this->counter = $counter;
    }

    public function toString()
    {
        return 'From ' . $this->get('from') . ', take flight ' . $this->flight . ' to ' . $this->get('to') . '. Gate ' . $this->gate . ', seat ' . $this->get('seat') . '. ' . ($this->counter ? 'Baggage drop at ticket counter ' . $this->counter . '.' : 'Baggage will be automatically transferred from your last leg.');
    }

}
