<?php
namespace TravelTrip\Trips;

class AirportBusTrip extends AbstractTrip
{

    function __construct($from, $to, $seat = null)
    {
        parent::__construct($from, $to, $seat);
    }

    public function toString()
    {
        return 'Take the airport bus from ' . $this->get('from') . ' to ' . $this->get('to') . '. ' . ($this->get('seat') ? 'Sit in seat ' . $this->get('seat') . '.' : 'No seat assignment.');
    }

}



