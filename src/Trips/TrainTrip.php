<?php
namespace TravelTrip\Trips;

class TrainTrip extends AbstractTrip
{

    private $train;

    function __construct($from, $to, $seat, $train)
    {
        parent::__construct($from, $to, $seat);

        $this->train = $train;
    }

    public function toString()
    {
        return 'Take train ' . $this->train . ' from ' . $this->get('from') . ' to ' . $this->get('to') . '. Sit in seat ' . $this->get('seat') . '.';
    }

}

