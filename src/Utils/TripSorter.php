<?php

namespace TravelTrip\Utils;

class TripSorter
{

    public function sort(array $items)
    {

        $fromIndex = $this->createDepartureIndex($items);
        $toIndex = $this->createArrivalIndex($items);

        $startingLocation = $this->getStartingLocation($items, $toIndex);

        $sortedBaseBoardingCards = array();
        $currentLocation = $startingLocation;

        while ($currentBaseBoardingCard = (array_key_exists($currentLocation, $fromIndex)) ? $fromIndex[$currentLocation] : null) {

            array_push($sortedBaseBoardingCards, $currentBaseBoardingCard);

            $currentLocation = $currentBaseBoardingCard->get('to');
        }

        return $sortedBaseBoardingCards;
    }

    private function createDepartureIndex($boardingCards)
    {
        $departureIndex = [];
        foreach ($boardingCards as $baseBoardingCard) {
            $departureIndex[$baseBoardingCard->get('from')] = $baseBoardingCard;
        }

        return $departureIndex;
    }

    private function createArrivalIndex($boardingCards)
    {
        $arrivalIndex = [];
        foreach ($boardingCards as $baseBoardingCard) {
            $arrivalIndex[$baseBoardingCard->get('to')] = $baseBoardingCard;
        }

        return $arrivalIndex;
    }

    private function getStartingLocation($baseBoardingCards, $arrivalIndex)
    {
        foreach($baseBoardingCards as $baseBoardingCard){

            $departureLocation = $baseBoardingCard->get('from');

            if (!array_key_exists($departureLocation, $arrivalIndex)) {
                return $departureLocation;
            }
        }
        return null;
    }
}
